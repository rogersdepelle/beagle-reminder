# Beagle Reminder App

* API URL: [beagle-reminder.tk/api](beagle-reminder.tk/api)
* API Docs: [https://app.swaggerhub.com/apis-docs/rogerspelle/beagle-reminder-api/1.0.0](https://app.swaggerhub.com/apis-docs/rogerspelle/beagle-reminder-api/1.0.0)
* Api Token (Authorize button): `Token b497a0dc1783fd1a2b9143a89ad2a79bfe98bea0`
* CI/CD Pipelines: (gitlab.com/rogersdepelle/beagle-reminder/-/pipelines)[https://gitlab.com/rogersdepelle/beagle-reminder/-/pipelines]

## Development

**Requirements:** [Docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script) [Docker Compose](https://docs.docker.com/compose/install/)
```
docker-compose up
```

**Load Authentication Data:**
```
docker-compose exec django python manage.py loaddata core/fixtures/auth.json
```

* Django Admin: [127.0.0.1:8008/admin/](http://127.0.0.1:8008/admin/)
* Admin username|password: `root|changeme123`
* Api Token: `Token b497a0dc1783fd1a2b9143a89ad2a79bfe98bea0`

**Run tests:**
```
docker-compose exec django python manage.py test
```

**pre-commit install**
```
virtualenv venv
source venv/bin/activate
pip install -r django/core/requirements/dev.txt
pre-commit install
```

## Deployment

* [Create EC2 Instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EC2_GetStarted.html)
* [Install GitLab Runner](https://docs.gitlab.com/runner/install/linux-manually.html)
* [Register a runner](https://docs.gitlab.com/runner/register/index.html)
* Remove .bash_logout: `sudo rm /home/gitlab-runner/.bash_logout`
