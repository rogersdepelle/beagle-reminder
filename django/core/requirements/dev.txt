-r main.txt
bandit==1.7.0
black==20.8b1
coverage==5.5
flake8==3.9.0
freezegun==1.1.0
ipython==7.21.0
pre-commit==2.11.1
safety==1.10.3
