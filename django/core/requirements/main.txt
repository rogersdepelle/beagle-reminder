celery==5.1.2
Django==3.2.8
django-filter==21.1
djangorestframework==3.12.4
gunicorn==20.0.4
psycopg2-binary==2.8.6
redis==3.5.3
