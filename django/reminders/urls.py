from django.urls import include, path
from rest_framework import routers

from reminders import views

router = routers.DefaultRouter()
router.register(r"reminders", views.ReminderViewSet, basename="reminders")
router.register(r"alerts", views.AlertViewSet, basename="alerts")
urlpatterns = [
    path("", include(router.urls)),
]
