from rest_framework import viewsets

from reminders.models import Reminder, Alert
from reminders.serializers import ReminderSerializer, AlertSerializer


class ReminderViewSet(viewsets.ModelViewSet):
    queryset = Reminder.objects.all().order_by("-datetime")
    serializer_class = ReminderSerializer
    filterset_fields = ["user_email"]


class AlertViewSet(viewsets.ModelViewSet):
    queryset = Alert.objects.all().order_by("-datetime")
    serializer_class = AlertSerializer
    filterset_fields = ["reminder"]
