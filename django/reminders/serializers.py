from rest_framework import serializers

from reminders.models import Reminder, Alert


class ReminderSerializer(serializers.ModelSerializer):
    alerts = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = Reminder
        fields = "__all__"


class AlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alert
        fields = "__all__"
