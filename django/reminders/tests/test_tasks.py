from django.test import override_settings
from django.utils import timezone
from django.core import mail

from freezegun import freeze_time

from reminders.models import Alert
from reminders.tests.test_api import BaseAPITestCase
from reminders.tasks import send_alerts_task


@override_settings(USE_TZ=False)
@freeze_time("2000-01-02 00:00:00")
class SendAlertsTaskTestCase(BaseAPITestCase):
    def test_send_alerts_task(self):
        now = timezone.now()
        alerts = Alert.objects.filter(sent=False, datetime__lte=now, reminder__datetime__gte=now)
        self.assertEqual(alerts.count(), 2)
        send_alerts_task()
        self.assertEqual(len(mail.outbox), 2)
        alerts = Alert.objects.filter(sent=False, datetime__lte=now, reminder__datetime__gte=now)
        self.assertEqual(alerts.count(), 0)
