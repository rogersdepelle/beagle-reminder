from django.contrib.auth.models import User
from django.test import override_settings

from freezegun import freeze_time
from rest_framework import status
from rest_framework.test import APITestCase

from reminders.models import Reminder, Alert


class BaseAPITestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create(username="user1")
        cls.reminder1 = Reminder.objects.create(
            id="ee7b855c-e335-4754-9901-f6da167b5078",
            title="Reminder 1",
            description="Desc 1",
            datetime="2000-01-02 01:00:00",
            user_email="test1@site.com",
        )
        cls.reminder2 = Reminder.objects.create(
            id="e1e3f613-285f-47af-8cb7-48cfa4b540a6",
            title="Reminder 2",
            description="Desc 2",
            datetime="2000-01-02 01:00:00",
            user_email="test2@site.com",
        )
        cls.alert1 = Alert.objects.create(
            id="e72dcb07-591b-4b1e-a172-882f9d0c7f9e",
            reminder=cls.reminder1,
            datetime="2000-01-01 00:30:00",
        )
        cls.alert2 = Alert.objects.create(
            id="7d5ec329-b5d4-4e61-a888-94a002aed300",
            reminder=cls.reminder1,
            datetime="2000-01-01 00:45:00",
        )
        cls.alert3 = Alert.objects.create(
            id="11a2a235-b7f8-4b27-bc67-5ad3711fb01d",
            reminder=cls.reminder2,
            datetime="2000-01-02 00:30:00",
        )


@override_settings(USE_TZ=False)
@freeze_time("2000-01-01 01:00:00")
class ReminderAPITestCase(BaseAPITestCase):
    def setUp(self):
        self.client.force_authenticate(user=self.user)

    def test_reminder_detail(self):
        response = self.client.get("/api/reminders/ee7b855c-e335-4754-9901-f6da167b5078/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = {
            "id": "ee7b855c-e335-4754-9901-f6da167b5078",
            "alerts": ["2000-01-01 00:30:00", "2000-01-01 00:45:00"],
            "user_email": "test1@site.com",
            "title": "Reminder 1",
            "datetime": "2000-01-02 01:00:00",
            "description": "Desc 1",
        }
        self.assertEqual(response.json(), expected_data)

    def test_reminder_list(self):
        response = self.client.get("/api/reminders/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = [
            {
                "id": "ee7b855c-e335-4754-9901-f6da167b5078",
                "alerts": ["2000-01-01 00:30:00", "2000-01-01 00:45:00"],
                "user_email": "test1@site.com",
                "title": "Reminder 1",
                "datetime": "2000-01-02 01:00:00",
                "description": "Desc 1",
            },
            {
                "id": "e1e3f613-285f-47af-8cb7-48cfa4b540a6",
                "alerts": ["2000-01-02 00:30:00"],
                "user_email": "test2@site.com",
                "title": "Reminder 2",
                "datetime": "2000-01-02 01:00:00",
                "description": "Desc 2",
            },
        ]
        self.assertEqual(response.json(), expected_data)

    def test_reminder_create(self):
        data = {
            "title": "Reminder 3",
            "user_email": "test3@site.com",
            "datetime": "2000-01-03 01:00:00",
        }
        response = self.client.post("/api/reminders/", data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response_data = response.json()
        self.assertEqual(response_data["title"], data["title"])
        self.assertEqual(response_data["user_email"], data["user_email"])
        self.assertEqual(response_data["datetime"], data["datetime"])

    def test_reminder_create_past_date(self):
        data = {
            "title": "Reminder 3",
            "user_email": "test3@site.com",
            "datetime": "1999-01-01 01:00:00",
        }
        response = self.client.post("/api/reminders/", data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response_data = response.json()
        self.assertEqual(
            response_data["datetime"][0],
            "Ensure this value is greater than or equal to 2000-01-01 01:00:00.",
        )

    def test_reminder_update(self):
        response = self.client.patch(
            "/api/reminders/ee7b855c-e335-4754-9901-f6da167b5078/",
            data={"datetime": "2000-01-04 01:00:00"},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = {
            "id": "ee7b855c-e335-4754-9901-f6da167b5078",
            "alerts": ["2000-01-01 00:30:00", "2000-01-01 00:45:00"],
            "user_email": "test1@site.com",
            "title": "Reminder 1",
            "datetime": "2000-01-04 01:00:00",
            "description": "Desc 1",
        }
        self.assertEqual(response.json(), expected_data)

    def test_reminder_delete(self):
        response = self.client.delete(
            "/api/reminders/ee7b855c-e335-4754-9901-f6da167b5078/",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Reminder.objects.count(), 1)

    def test_reminder_list_by_email_user(self):
        response = self.client.get("/api/reminders/?user_email=test1@site.com")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = [
            {
                "id": "ee7b855c-e335-4754-9901-f6da167b5078",
                "alerts": ["2000-01-01 00:30:00", "2000-01-01 00:45:00"],
                "user_email": "test1@site.com",
                "title": "Reminder 1",
                "datetime": "2000-01-02 01:00:00",
                "description": "Desc 1",
            }
        ]
        self.assertEqual(response.json(), expected_data)


@override_settings(USE_TZ=False)
@freeze_time("2000-01-01 01:00:00")
class AlertAPITestCase(BaseAPITestCase):
    def setUp(self):
        self.client.force_authenticate(user=self.user)

    def test_alert_detail(self):
        response = self.client.get("/api/alerts/e72dcb07-591b-4b1e-a172-882f9d0c7f9e/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = {
            "id": "e72dcb07-591b-4b1e-a172-882f9d0c7f9e",
            "datetime": "2000-01-01 00:30:00",
            "reminder": "ee7b855c-e335-4754-9901-f6da167b5078",
            "sent": False,
        }
        self.assertEqual(response.json(), expected_data)

    def test_alert_list(self):
        response = self.client.get("/api/alerts/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = [
            {
                "id": "11a2a235-b7f8-4b27-bc67-5ad3711fb01d",
                "datetime": "2000-01-02 00:30:00",
                "reminder": "e1e3f613-285f-47af-8cb7-48cfa4b540a6",
                "sent": False,
            },
            {
                "id": "7d5ec329-b5d4-4e61-a888-94a002aed300",
                "datetime": "2000-01-01 00:45:00",
                "reminder": "ee7b855c-e335-4754-9901-f6da167b5078",
                "sent": False,
            },
            {
                "id": "e72dcb07-591b-4b1e-a172-882f9d0c7f9e",
                "datetime": "2000-01-01 00:30:00",
                "reminder": "ee7b855c-e335-4754-9901-f6da167b5078",
                "sent": False,
            },
        ]
        self.assertEqual(response.json(), expected_data)

    def test_alert_create(self):
        data = {
            "datetime": "2000-01-02 00:45:00",
            "reminder": "e1e3f613-285f-47af-8cb7-48cfa4b540a6",
        }
        response = self.client.post("/api/alerts/", data=data)
        response_data = response.json()
        self.assertEqual(response_data["reminder"], data["reminder"])
        self.assertEqual(response_data["datetime"], data["datetime"])
        self.assertEqual(response_data["sent"], False)

    def test_alert_create_past_date(self):
        data = {
            "datetime": "1999-01-01 01:00:00",
            "reminder": "e1e3f613-285f-47af-8cb7-48cfa4b540a6",
        }
        response = self.client.post("/api/alerts/", data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        response_data = response.json()
        self.assertEqual(
            response_data["datetime"][0],
            "Ensure this value is greater than or equal to 2000-01-01 01:00:00.",
        )

    def test_alert_update(self):
        response = self.client.patch(
            "/api/alerts/e72dcb07-591b-4b1e-a172-882f9d0c7f9e/",
            data={"datetime": "2000-01-02 00:00:00"},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = {
            "id": "e72dcb07-591b-4b1e-a172-882f9d0c7f9e",
            "datetime": "2000-01-02 00:00:00",
            "reminder": "ee7b855c-e335-4754-9901-f6da167b5078",
            "sent": False,
        }
        self.assertEqual(response.json(), expected_data)

    def test_alert_delete(self):
        response = self.client.delete(
            "/api/alerts/e72dcb07-591b-4b1e-a172-882f9d0c7f9e/",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Alert.objects.count(), 2)

    def test_alert_list_by_reminder(self):
        response = self.client.get("/api/alerts/?reminder=ee7b855c-e335-4754-9901-f6da167b5078")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = [
            {
                "id": "7d5ec329-b5d4-4e61-a888-94a002aed300",
                "datetime": "2000-01-01 00:45:00",
                "reminder": "ee7b855c-e335-4754-9901-f6da167b5078",
                "sent": False,
            },
            {
                "id": "e72dcb07-591b-4b1e-a172-882f9d0c7f9e",
                "datetime": "2000-01-01 00:30:00",
                "reminder": "ee7b855c-e335-4754-9901-f6da167b5078",
                "sent": False,
            },
        ]
        self.assertEqual(response.json(), expected_data)
