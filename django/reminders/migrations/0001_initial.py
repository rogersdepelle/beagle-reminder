# Generated by Django 3.2.8 on 2021-10-26 20:53
import uuid

import django.core.validators
import django.db.models.deletion
import django.utils.timezone

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Reminder",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                ("user_email", models.EmailField(max_length=254)),
                ("title", models.CharField(max_length=50)),
                (
                    "datetime",
                    models.DateTimeField(
                        validators=[django.core.validators.MinValueValidator(limit_value=django.utils.timezone.now)]
                    ),
                ),
                ("description", models.TextField(blank=True, null=True)),
            ],
            options={
                "verbose_name": "Reminder",
                "verbose_name_plural": "Reminders",
            },
        ),
        migrations.CreateModel(
            name="Alert",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                (
                    "datetime",
                    models.DateTimeField(
                        validators=[django.core.validators.MinValueValidator(limit_value=django.utils.timezone.now)]
                    ),
                ),
                ("sent", models.BooleanField(default=False)),
                (
                    "reminder",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="alerts",
                        to="reminders.reminder",
                    ),
                ),
            ],
            options={
                "verbose_name": "Alert",
                "verbose_name_plural": "Alerts",
                "unique_together": {("reminder", "datetime")},
            },
        ),
    ]
