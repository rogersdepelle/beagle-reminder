from django.contrib import admin

from reminders.models import Reminder, Alert


admin.site.register(Reminder)
admin.site.register(Alert)
