from celery import shared_task
from celery.utils.log import get_task_logger

from django.conf import settings
from django.core.mail import send_mass_mail
from django.utils import timezone


from reminders.models import Alert

logger = get_task_logger(__name__)


@shared_task
def send_alerts_task():
    logger.info("send_alerts_task started!")
    now = timezone.now()
    alerts = Alert.objects.filter(sent=False, datetime__lte=now, reminder__datetime__gte=now)
    alerts_count = alerts.count()

    if alerts_count == 0:
        logger.info("0 alerts to send!")
        return

    emails = []
    for alert in alerts:
        reminder = alert.reminder
        subject = f"Reminder: {reminder.title} in XXX hours"
        message = f"Title: {reminder.title}\n\nDate: {reminder.datetime}\n\nDescription: {reminder.description}"
        logger.info((subject, message, settings.DEFAULT_FROM_EMAIL, [reminder.user_email]))
        emails.append((subject, message, settings.DEFAULT_FROM_EMAIL, [reminder.user_email]))
    delivered_messages = send_mass_mail(emails)

    if delivered_messages == alerts_count:
        alerts.update(sent=True)
        logger.info(f"send_alerts_task finished! {delivered_messages} sent.")
    else:
        logger.warning("Delivered messages less than expected!")
