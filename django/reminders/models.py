import uuid

from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone


class Reminder(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_email = models.EmailField(max_length=254)
    title = models.CharField(max_length=50)
    datetime = models.DateTimeField(validators=[MinValueValidator(limit_value=timezone.now)])
    description = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = "Reminder"
        verbose_name_plural = "Reminders"

    def __str__(self):
        return self.title


class Alert(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    reminder = models.ForeignKey("reminders.Reminder", on_delete=models.CASCADE, related_name="alerts")
    datetime = models.DateTimeField(validators=[MinValueValidator(limit_value=timezone.now)])
    sent = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Alert"
        verbose_name_plural = "Alerts"
        unique_together = ["reminder", "datetime"]

    def __str__(self):
        return self.datetime.strftime(settings.DATETIME_FORMAT)
