#!/bin/sh

python manage.py migrate --fake-initial

if [ "$MODE" = "production" ]; then
  python manage.py collectstatic --noinput -v 0
fi

exec "$@"
